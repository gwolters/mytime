//
//  main.m
//  MyTime
//
//  Created by Geri Wolters on 03-09-12.
//  Copyright (c) 2012 Geri Wolters. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
