//
//  AppceteraAppDelegate.h
//  MyTime
//
//  Created by Geri Wolters on 03-09-12.
//  Copyright (c) 2012 Geri Wolters. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppceteraAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)saveAction:(id)sender;

@end
